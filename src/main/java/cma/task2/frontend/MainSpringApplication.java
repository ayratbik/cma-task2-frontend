package cma.task2.frontend;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainSpringApplication {

    public static void main(String[] args) {
        Application.launch(JavaFxApplication.class, args);
    }

}
