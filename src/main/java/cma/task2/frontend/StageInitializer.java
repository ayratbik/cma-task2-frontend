package cma.task2.frontend;

import cma.task2.frontend.JavaFxApplication.StageReadyEvent;
import cma.task2.frontend.javafx.BackendProblemController;
import cma.task2.frontend.javafx.StudentsMainController;
import cma.task2.frontend.restclient.StudentClient;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxWeaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StageInitializer implements ApplicationListener<StageReadyEvent> {

    private static final Logger log = LoggerFactory.getLogger(StageInitializer.class);

    private final String applicationTitle;
    private final FxWeaver fxWeaver;

    @Autowired
    StudentClient studentClient;

    public StageInitializer(@Value("${spring.application.ui.title}") String applicationTitle,
                            FxWeaver fxWeaver) {
        this.applicationTitle = applicationTitle;
        this.fxWeaver = fxWeaver;
    }

    @Override
    public void onApplicationEvent(StageReadyEvent event) {
        Stage stage = event.getStage();
        try {
            int count = studentClient.getAll().size();
            log.info("found {} students in DB", count);
            stage.setScene(new Scene(fxWeaver.loadView(StudentsMainController.class), 800, 600));
            stage.setMinHeight(500);
            stage.setMinWidth(700);
        } catch (Exception ex) {
            stage.setScene(new Scene(fxWeaver.loadView(BackendProblemController.class)));
            stage.setResizable(false);
        }
        stage.setTitle(applicationTitle);
        stage.show();
    }
}
