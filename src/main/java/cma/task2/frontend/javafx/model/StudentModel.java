package cma.task2.frontend.javafx.model;

import cma.task2.frontend.restclient.Student;

import java.util.Optional;

public class StudentModel {

    private Student student;

    public StudentModel() {
        clear();
    }

    public StudentModel(Student student) {
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void clear() {
        student = null;
    }

    public boolean isNewStudentValid() {
        return student != null && student.getBirthDay() != null
                && !Optional.ofNullable(student.getFirstName()).orElse("").isBlank()
                && !Optional.ofNullable(student.getStudyGroup()).orElse("").isBlank()
                && !Optional.ofNullable(student.getKeyNumber()).orElse("").isBlank()
                && !Optional.ofNullable(student.getLastName()).orElse("").isBlank()
                && !Optional.ofNullable(student.getPatronymic()).orElse("").isBlank();
    }

    public Student getNewStudent() {
        student = new Student();
        return student;
    }
}
