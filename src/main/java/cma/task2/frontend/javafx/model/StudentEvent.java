package cma.task2.frontend.javafx.model;

import cma.task2.frontend.restclient.Student;
import org.springframework.context.ApplicationEvent;

public class StudentEvent extends ApplicationEvent {

	private static final long serialVersionUID = -4724492620172856575L;

	public StudentEvent(Student source) {
        super(source);
    }
}
