package cma.task2.frontend.javafx;

import cma.task2.frontend.javafx.model.StudentEvent;
import cma.task2.frontend.javafx.model.StudentModel;
import cma.task2.frontend.restclient.Student;
import cma.task2.frontend.restclient.StudentClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.controlsfx.control.textfield.CustomTextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@FxmlView("/fxml/student-card.fxml")
public class StudentCardController {

    private Stage stage;

    @FXML
    private GridPane gPaneRoot;

    @FXML
    private CustomTextField txtLastName;

    @FXML
    private CustomTextField txtFirstName;

    @FXML
    private CustomTextField txtPatronymic;

    @FXML
    private DatePicker dpBirthday;

    @FXML
    private CustomTextField txtStudyGroup;

    @FXML
    private CustomTextField txtKeyNumber;

    @FXML
    private Label lbTitle;

    @FXML
    private Button btnExit;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCreate;

    @FXML
    private HBox hBoxButtons;

    @Autowired
    private StudentModel studentModel;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private StudentClient studentClient;

    @FXML
    public void initialize() {
        this.stage = new Stage();
        stage.setScene(new Scene(gPaneRoot));
        stage.setMinWidth(380);
        stage.setMinHeight(400);
        stage.setMaxWidth(580);
        stage.setMaxHeight(540);
        stage.initModality(Modality.APPLICATION_MODAL);
        btnCreate.setOnAction(this::persistStudent);
        btnSave.setOnAction(this::saveStudent);
        btnExit.setOnAction(ev -> stage.close());
    }

    private void saveStudent(ActionEvent actionEvent) {
        Student student = studentModel.getStudent();
        fillStudent(student);
        try {
            studentClient.update(student);
            publisher.publishEvent(new StudentEvent(student));
            stage.close();
        }catch (Exception ex){
            showFailedToSaveDataAlert();
        }
    }

    private void showFailedToSaveDataAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Проблема сохранения");
        alert.setHeaderText("Не удалось сохранить данные");
        alert.setContentText("Проверьте введенные значения");
        alert.showAndWait();
    }

    private void persistStudent(ActionEvent actionEvent) {
        Student student = studentModel.getNewStudent();
        fillStudent(student);

        if (studentModel.isNewStudentValid()) {
            try {
                studentClient.add(student);
                publisher.publishEvent(new StudentEvent(student));
                stage.close();
            }catch (Exception ex){
                showFailedToSaveDataAlert();
            }
        } else {
            showNotEnoughDataWarn();
        }
    }

    private void showNotEnoughDataWarn() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Не достаточно данных");
        alert.setHeaderText("Не достаточно данных для регистрации");
        alert.setContentText("Для регистрации студента необходимо заполнить все поля");
        alert.showAndWait();
    }

    public void show() {

        Student student = studentModel.getStudent();

        if (student == null) {
            lbTitle.setText("Регистрация новой записи");
            if (!hBoxButtons.getChildren().contains(btnCreate))
                hBoxButtons.getChildren().add(0, btnCreate);
            if (hBoxButtons.getChildren().contains(btnSave))
                hBoxButtons.getChildren().remove(btnSave);

            clear();
        } else {
            lbTitle.setText("Редактирование записи");
            if (!hBoxButtons.getChildren().contains(btnSave))
                hBoxButtons.getChildren().add(0, btnSave);
            if (hBoxButtons.getChildren().contains(btnCreate))
                hBoxButtons.getChildren().remove(btnCreate);

            fillFields(student);
        }

        stage.showAndWait();
    }

    private void clear() {
        txtFirstName.clear();
        txtLastName.clear();
        txtPatronymic.clear();
        txtKeyNumber.clear();
        txtStudyGroup.clear();
        dpBirthday.setValue(null);
    }

    private void fillFields(Student student) {
        txtFirstName.setText(student.getFirstName());
        txtLastName.setText(student.getLastName());
        txtPatronymic.setText(student.getPatronymic());
        txtKeyNumber.setText(student.getKeyNumber());
        txtStudyGroup.setText(student.getStudyGroup());
        dpBirthday.setValue(student.getBirthDay());
    }

    public void fillStudent(Student student) {

        student.setFirstName(txtFirstName.getText());
        student.setLastName(txtLastName.getText());
        student.setPatronymic(txtPatronymic.getText());
        student.setKeyNumber(txtKeyNumber.getText());
        student.setStudyGroup(txtStudyGroup.getText());
        student.setBirthDay(dpBirthday.getValue());
    }
}
