package cma.task2.frontend.javafx;

import cma.task2.frontend.javafx.model.StudentEvent;
import cma.task2.frontend.javafx.model.StudentModel;
import cma.task2.frontend.restclient.Student;
import cma.task2.frontend.restclient.StudentClient;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxControllerAndView;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@FxmlView("/fxml/students-table.fxml")
public class StudentsMainController implements ApplicationListener<StudentEvent> {

    @FXML
    private TableView<Student> tvStudents;

    @FXML
    private TableColumn<Student, String> clmKeyNumber;

    @FXML
    private TableColumn<Student, String> clmStudyGroup;

    @FXML
    private TableColumn<Student, String> clmFullName;

    @FXML
    private TableColumn<Student, String> clmBirthday;

    @FXML
    private Button btnExit;

    @Autowired
    StudentClient studentClient;

    @Autowired
    StudentModel studentModel;

    private final FxControllerAndView<StudentCardController, GridPane> studentCard;

    private ObservableList<Student> students;

    @FXML
    void onAddRequest(ActionEvent event) {
        editStudent(null);
    }

    @FXML
    void onChangeRequest(ActionEvent event) {
        editStudent(tvStudents.getSelectionModel().getSelectedItem());
    }

    private void editStudent(Student selectedItem) {
        studentModel.setStudent(selectedItem);
        studentCard.getController().show();
    }

    @FXML
    void onRemoveRequest(ActionEvent event) {
        //backendAlert.getController().show();
        Student student = tvStudents.getSelectionModel().getSelectedItem();
        if (student != null)
            studentClient.delete(student.getId());
        refreshTable();
    }

    public StudentsMainController(FxControllerAndView<StudentCardController, GridPane> studentCard) {
        this.studentCard = studentCard;
    }

    @FXML
    public void initialize() {
        btnExit.setOnAction(ev ->
                ((Stage) (btnExit.getScene().getWindow())).close()
        );

        students = FXCollections.observableArrayList();
        tvStudents.setItems(students);
        refreshTable();

        clmFullName.setCellValueFactory(cell -> new SimpleStringProperty(
                cell.getValue().getLastName() + " "
                        + cell.getValue().getFirstName() + " "
                        + cell.getValue().getPatronymic()
        ));
        clmBirthday.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getBirthDay().toString()));
        clmKeyNumber.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getKeyNumber()));
        clmStudyGroup.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getStudyGroup()));

        tvStudents.setRowFactory(tv -> {
            TableRow<Student> row = new TableRow<>();
            row.setOnMouseClicked(ev -> {
                if (ev.getClickCount() == 2 && (!row.isEmpty())) {
                    editStudent(row.getItem());
                }
            });
            return row;
        });
    }

    @Override
    public void onApplicationEvent(StudentEvent event) {
//        btnExit.setText(((Student) event.getSource()).getFirstName());
        refreshTable();
    }

    private void refreshTable() {
        List<Student> studentList = studentClient.getAll();
        students.clear();
        students.addAll(studentList);
    }
}
