package cma.task2.frontend.javafx;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.stereotype.Component;

@Component
@FxmlView("/fxml/no-connection-error.fxml")
public class BackendProblemController {

    @FXML
    private AnchorPane paneRoot;
    @FXML
    private Button btnExit;

    @FXML
    public void initialize() {

        btnExit.setOnAction(ev -> ((Stage) (btnExit.getScene().getWindow())).close());
    }
}
