package cma.task2.frontend.restclient;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class StudentClientImpl implements StudentClient {

	@Autowired
	RestTemplate restTemplate;

//    @Autowired
//    public StudentClientImpl(RestTemplateBuilder builder) {
//        this.restTemplate = builder.build();
//    }

	final String ROOT_URI = "http://localhost:8080/students";

	@Override
	public List<Student> getAll() {
		ResponseEntity<Student[]> response = restTemplate.getForEntity(ROOT_URI, Student[].class);
		return Arrays.asList(response.getBody());

	}

	@Override
	public Student getById(Long id) {
		ResponseEntity<Student> response = restTemplate.getForEntity(ROOT_URI + "/" + id, Student.class);
		return response.getBody();
	}

	@Override
	public Student add(Student student) {
		ResponseEntity<Student> response = restTemplate.postForEntity(ROOT_URI, student, Student.class);
		return response.getBody();
	}

	@Override
	public void update(Student student) {
		restTemplate.put(ROOT_URI + "/" + student.getId(), student);
	}

	@Override
	public void delete(Long id) {
		restTemplate.delete(ROOT_URI + "/" + id);

	}
}
