package cma.task2.frontend.restclient;

import java.util.List;

public interface RestClient<T> {
    List<T> getAll();

    T getById(Long id);

    T add(T dto);

    void update(T dto);

    void delete(Long id);
}
